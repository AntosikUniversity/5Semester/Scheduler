﻿using System;

namespace Scheduler.Helpers
{
    public static class PeriodCheck
    {
        public static bool IsDayPeriod(DateTime first, DateTime second, uint period)
        {
            return (first.Date - second.Date).TotalDays % period == 0;
        }

        public static bool IsWeekPeriod(DateTime first, DateTime second, uint period)
        {
            return (first.Date - second.Date).TotalDays / 7 % period == 0;
        }

        public static bool IsMonthPeriod(DateTime first, DateTime second, uint period)
        {
            return (first.Month - second.Month) % period == 0 && first.Day == second.Day;
        }

        public static bool IsYearPeriod(DateTime first, DateTime second, uint period)
        {
            return (first.Year - second.Year) % period == 0 && first.DayOfYear == second.DayOfYear;
        }
    }
}