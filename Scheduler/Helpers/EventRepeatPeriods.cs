﻿namespace Scheduler.Helpers
{
    public enum EventRepeatPeriods
    {
        None = 0,
        Day = 1,
        Week = 2,
        Month = 3,
        Year = 4
    }
}