﻿namespace Scheduler.Helpers
{
    public enum SearchTypes
    {
        ByDate,
        ByName
    }
}