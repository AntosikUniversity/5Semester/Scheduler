﻿using Scheduler.Helpers;

namespace Scheduler.Interfaces
{
    public interface IRepeatableEvent : IEvent
    {
        uint PeriodNum { get; set; }
        EventRepeatPeriods PeriodType { get; set; }
    }
}