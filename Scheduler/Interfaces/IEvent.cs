﻿using System;

namespace Scheduler.Interfaces
{
    public interface IEvent
    {
        DateTime DateTime { get; set; }
        string Description { get; set; }
    }
}