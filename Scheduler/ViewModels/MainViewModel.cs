﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Scheduler.Helpers;

namespace Scheduler.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        private bool _isEditable;
        private object _searchParameter;
        private SearchTypes _searchType;
        private EventViewModel _tempEvent;
        public EventViewModel SelectedEvent;

        public MainViewModel()
        {
            Events = new ObservableCollection<EventViewModel>();
            EventsFound = new ObservableCollection<EventViewModel>(Events);

            TempEvent = null;
            SelectedEvent = null;
            IsEditable = false;

            NewEvent = new Command(CreateNewEvent);
            RunSearch = new Command(data =>
            {
                if (data is string text) Search(SearchTypes.ByName, text);
            });
            SelectEvent = new Command(data =>
            {
                if (data is EventViewModel selectedEvent)
                {
                    TempEvent = selectedEvent;
                    IsEditable = true;
                }
            });
            DeleteEvent = new Command(data =>
            {
                if (data is EventViewModel selectedEvent)
                    Events.Remove(selectedEvent);
            });

            /* SEARCH RELATED */
            SearchType = SearchTypes.ByName;
            SearchParameter = "";
            Events.CollectionChanged += (o, args) => Search(SearchType, SearchParameter);
        }

        public EventViewModel TempEvent
        {
            get => _tempEvent;
            set => Change(ref _tempEvent, value);
        }

        public ObservableCollection<EventViewModel> Events { get; }
        public ObservableCollection<EventViewModel> EventsFound { get; }

        public ICommand NewEvent { get; }
        public ICommand RunSearch { get; }
        public ICommand SelectEvent { get; }
        public ICommand DeleteEvent { get; }

        public SearchTypes SearchType
        {
            get => _searchType;
            set => Change(ref _searchType, value);
        }

        public object SearchParameter
        {
            get => _searchParameter;
            set => Change(ref _searchParameter, value);
        }

        public bool IsEditable
        {
            get => _isEditable;
            set => Change(ref _isEditable, value);
        }

        private void CreateNewEvent()
        {
            if (IsEditable)
            {
                Events.Add(TempEvent);
                Search(SearchType, SearchParameter);
                TempEvent = new EventViewModel(DateTime.Now, "");
                IsEditable = false;
            }
            else
            {
                IsEditable = true;
            }
        }

        public void Search(SearchTypes type, object parameter)
        {
            if (type == SearchTypes.ByDate)
            {
                if (!(parameter is DateTime dateSearch)) return;

                SearchType = SearchTypes.ByName;
                SearchParameter = dateSearch.ToString("dd.MM.yyyy");
                EventsFound.Clear();

                var found = Events.Where(currentEvent =>
                {
                    if (!currentEvent.HasPeriod)
                        return currentEvent.DateTime.Date == dateSearch.Date;

                    switch (currentEvent.PeriodType)
                    {
                        case EventRepeatPeriods.Day:
                            return PeriodCheck.IsDayPeriod(currentEvent.DateTime, dateSearch,
                                currentEvent.PeriodNum);
                        case EventRepeatPeriods.Week:
                            return PeriodCheck.IsWeekPeriod(currentEvent.DateTime, dateSearch,
                                currentEvent.PeriodNum);
                        case EventRepeatPeriods.Month:
                            return PeriodCheck.IsMonthPeriod(currentEvent.DateTime, dateSearch,
                                currentEvent.PeriodNum);
                        case EventRepeatPeriods.Year:
                            return PeriodCheck.IsYearPeriod(currentEvent.DateTime, dateSearch,
                                currentEvent.PeriodNum);
                        default:
                            return false;
                    }
                });

                foreach (var eventFound in found)
                    EventsFound.Add(eventFound);
            }
            else
            {
                if (!(parameter is string textSearch)) return;

                SearchType = type;
                SearchParameter = textSearch;
                EventsFound.Clear();

                if (DateTime.TryParse(textSearch, out var date))
                {
                    Search(SearchTypes.ByDate, date);
                    return;
                }

                var found = Events.Where(currentEvent => currentEvent.Description.Contains(textSearch));

                foreach (var eventFound in found)
                    EventsFound.Add(eventFound);
            }
        }
    }
}