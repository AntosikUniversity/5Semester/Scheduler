﻿using System;
using Scheduler.Helpers;
using Scheduler.Interfaces;

namespace Scheduler.ViewModels
{
    public class EventViewModel : ViewModelBase, IRepeatableEvent
    {
        private DateTime _dateTime;
        private string _description;
        private uint _periodNum;
        private EventRepeatPeriods _periodType;

        public EventViewModel(DateTime dateTime, string description, uint periodNum = 0,
            EventRepeatPeriods periodType = EventRepeatPeriods.None)
        {
            DateTime = dateTime;
            Description = description;
            PeriodNum = periodNum;
            PeriodType = periodType;
        }

        public bool HasPeriod => PeriodNum != 0 && PeriodType != EventRepeatPeriods.None;

        public string PeriodDescription => PeriodNum == 0 || PeriodType == EventRepeatPeriods.None
            ? ""
            : $"Every {PeriodNum} {PeriodType}";

        public DateTime DateTime
        {
            get => _dateTime;
            set => Change(ref _dateTime, value);
        }

        public string Description
        {
            get => _description;
            set => Change(ref _description, value);
        }

        public uint PeriodNum
        {
            get => _periodNum;
            set => Change(ref _periodNum, value, new[] {"PeriodNum", "PeriodDescription", "HasPeriod"});
        }

        public EventRepeatPeriods PeriodType
        {
            get => _periodType;
            set => Change(ref _periodType, value, new[] {"PeriodType", "PeriodDescription", "HasPeriod"});
        }
    }
}