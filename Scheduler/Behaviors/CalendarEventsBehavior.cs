﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Shapes;
using Scheduler.Helpers;
using Scheduler.ViewModels;

namespace Scheduler.Behaviors
{
    public class CalendarEventsBehavior : Behavior<Calendar>
    {
        private const int DaysCount = 42; // 7 * 6 = 42 items

        // Overrided OnAttached Behavior method
        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.Loaded += InitEvents;
            AssociatedObject.DisplayDateChanged += OnDisplayChange;
            AssociatedObject.PreviewMouseDoubleClick += RunSearch;
            AssociatedObject.PreviewMouseDown += SelectDate;
        }

        private void SelectDate(object sender, MouseButtonEventArgs e)
        {
            if (!(AssociatedObject.DataContext is MainViewModel data)) return;
            if (!(e.OriginalSource is Path path && path.DataContext is DateTime selected)) return;

            if (data.TempEvent == null)
                data.TempEvent = new EventViewModel(selected, "");
            else
                data.TempEvent.DateTime = new DateTime(selected.Year, selected.Month, selected.Day,
                    data.TempEvent.DateTime.Hour, data.TempEvent.DateTime.Minute, data.TempEvent.DateTime.Second);
            e.Handled = true;
        }

        // AssociatedObject.MouseDoubleClick EventHandler (run search on date)
        private void RunSearch(object sender, MouseButtonEventArgs e)
        {
            if (!(AssociatedObject.DataContext is MainViewModel data)) return;
            if (!(e.OriginalSource is Path path && path.DataContext is DateTime selected)) return;

            data.Search(SearchTypes.ByDate, selected);
            e.Handled = true;
        }

        // Overrided OnDetaching Behavior method
        protected override void OnDetaching()
        {
            AssociatedObject.PreviewMouseDown -= SelectDate;
            AssociatedObject.Loaded -= InitEvents;
            AssociatedObject.DisplayDateChanged -= OnDisplayChange;
            AssociatedObject.MouseDoubleClick -= RunSearch;

            if (AssociatedObject.DataContext is MainViewModel data)
                data.Events.CollectionChanged += UpdatedEvents;
            base.OnDetaching();
        }

        // Methods, which handles some events & update diplayed events

        #region EventsReloading

        // AssociatedObject.Loaded EventHandler
        private void InitEvents(object sender, RoutedEventArgs e)
        {
            LoadEvents(null);

            if (AssociatedObject.DataContext is MainViewModel data)
                data.Events.CollectionChanged += UpdatedEvents;
        }

        // AssociatedObject.DisplayDateChanged EventHandler
        private void OnDisplayChange(object sender, CalendarDateChangedEventArgs e)
        {
            LoadEvents(e?.AddedDate);
        }

        // Specify DataContext.Events ItemChanged EventHandler
        private void UpdatedEvents(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
                foreach (EventViewModel item in e.NewItems)
                    item.PropertyChanged += ItemChanged;

            if (e.OldItems != null)
                foreach (EventViewModel item in e.OldItems)
                    item.PropertyChanged -= ItemChanged;

            LoadEvents(null);
        }

        // DataContext.Events ItemChanged EventHandler
        private void ItemChanged(object sender, PropertyChangedEventArgs e)
        {
            LoadEvents(null);
        }

        #endregion

        // Methods, which load events, based on displayed dates

        #region GetEvents

        // Load events to BlackoutDates
        private void LoadEvents(DateTime? displayDate)
        {
            AssociatedObject.BlackoutDates.Clear();

            var firstDay = GetFirstDayInCalendar(displayDate);
            var eventsOnDates = CalculateEvents(firstDay);

            var eventDays = new HashSet<DateTime>(eventsOnDates.Keys);
            var newBlackoutDates = eventDays.Select(date => new CalendarDateRange(date)).ToList();
            try
            {
                foreach (var calendarDateRange in newBlackoutDates)
                    AssociatedObject.BlackoutDates.Add(calendarDateRange);
            }
            catch
            {
                // ignored
            }
        }

        // Returns events on displayed dates
        private Dictionary<DateTime, List<EventViewModel>> CalculateEvents(DateTime date)
        {
            var data = AssociatedObject.DataContext as MainViewModel;
            var results = new Dictionary<DateTime, List<EventViewModel>>();

            if (data == null)
                return results;

            for (var i = 0; i < DaysCount; i++)
            {
                var day = date.AddDays(i);
                var foundEvents = data.Events.Where(currentEvent =>
                {
                    if (!currentEvent.HasPeriod)
                        return currentEvent.DateTime.Date == day.Date;
                    switch (currentEvent.PeriodType)
                    {
                        case EventRepeatPeriods.Day:
                            return PeriodCheck.IsDayPeriod(currentEvent.DateTime, day,
                                currentEvent.PeriodNum);
                        case EventRepeatPeriods.Week:
                            return PeriodCheck.IsWeekPeriod(currentEvent.DateTime, day,
                                currentEvent.PeriodNum);
                        case EventRepeatPeriods.Month:
                            return PeriodCheck.IsMonthPeriod(currentEvent.DateTime, day,
                                currentEvent.PeriodNum);
                        case EventRepeatPeriods.Year:
                            return PeriodCheck.IsYearPeriod(currentEvent.DateTime, day,
                                currentEvent.PeriodNum);
                    }
                    return false;
                }).ToList();

                if (foundEvents.Count > 0)
                    results.Add(day, foundEvents);
            }

            return results;
        }

        // Returns first displayed date
        private DateTime GetFirstDayInCalendar(DateTime? loadedDate)
        {
            var displayDate = AssociatedObject.DisplayDate;
            var firstDay = loadedDate ?? new DateTime(displayDate.Year, displayDate.Month, 1);

            var delta = DayOfWeek.Monday - firstDay.DayOfWeek;
            delta = delta == 1 ? -6 : delta == 0 ? -7 : delta;
            return firstDay.AddDays(delta);
        }

        #endregion
    }
}